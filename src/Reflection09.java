import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflection09 {
  public static void main(String[] args) throws Exception {
    Reflector_main echo = new Reflector_main();
    Method[] ref_methods = echo.getClass().getMethods();
    System.out.printf("There are %d methods\n", ref_methods.length);

    for (Method ref_m : ref_methods) {
      System.out.printf("method name=%s type=%s parameters = ", ref_m.getName(),
          ref_m.getReturnType());
      Class[] types = ref_m.getParameterTypes();
      for (Class ref_class : types) {
        System.out.print(ref_class.getName() + " ");
      }
      System.out.println();
    }
  }
}
