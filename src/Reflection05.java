import java.lang.reflect.Field;

public class Reflection05 {
  public static void main(String[] args) throws Exception {
    Reflector_main echo = new Reflector_main();
    Field[] ref_fields = echo.getClass().getDeclaredFields();
    System.out.printf("There are %d fields\n", ref_fields.length);

    for (Field ref_f : ref_fields) {
      System.out.printf("field name=%s type=%s value=%d\n", ref_f.getName(),
          ref_f.getType(), ref_f.getByte(echo));
    }
  }
}
