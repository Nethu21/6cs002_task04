public class Reflector_main {

  public byte ref01 = 59;
  private byte ref02 = 97;

  public Reflector_main() {
  }

  public Reflector_main(byte e, byte n) {
    this.ref01 = e;
    this.ref02 = n;
  }


public void sq_E() {
    this.ref01 *= this.ref01;
  }

private void sq_N() {
    this.ref02 *= this.ref02;
  }

  public byte getE() {
    return ref01;
  }

  private void setE(byte e) {
    this.ref01 = e;
  }

  public byte getN() {
    return ref02;
  }

  public void setN(byte n) {
    this.ref02 = n;
  }

  public String toString() {
    return String.format("(ref01:%d, ref02:%d)", ref01, ref02);
  }
}
