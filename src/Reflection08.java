import java.lang.reflect.*;

public class Reflection08 {
  public static void main(String[] args) throws Exception {
    Reflector_main echo = new Reflector_main();
    Field[] ref_fields = echo.getClass().getDeclaredFields();
    System.out.printf("There are %d fields\n", ref_fields.length);
    for (Field ref_f : ref_fields) {
      ref_f.setAccessible(true);
      byte x = ref_f.getByte(echo);
      x++;
      ref_f.setByte(echo, x);
      System.out.printf("field name=%s type=%s value=%d\n", ref_f.getName(),
          ref_f.getType(), ref_f.getByte(echo));
    }
  }
}
