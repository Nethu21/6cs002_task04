package ref_KUnit;

import java.util.*;

public class Ref_Kunit {
  private static List<String> Ref_Checks;
  private static int Ref_checksMade = 0;
  private static int Ref_testPassed = 0;
  private static int Ref_testFailed = 0;

  private static void connectTo_Report(String txt) {
    if (Ref_Checks == null) {
      Ref_Checks = new LinkedList<String>();
    }
      Ref_Checks.add(String.format("%04d: %s", Ref_checksMade++, txt));
  }

  public static void scan_EqualVals(byte unit1, byte unit2) {
	    if (unit1 == unit2) {
	    	connectTo_Report(String.format("PASS - The value %d and %d are identical.", unit1, unit2));
	    	Ref_testPassed++;
	    } 
	    else {
	    	connectTo_Report(String.format("FAIL - The value %d and %d are NOT identical.", unit1, unit2));
	    	Ref_testFailed++;
	    }
  }

  public static void scan_NotEqualVals(byte unit1, byte unit2) {
	    if (unit1 != unit2) {
	    	connectTo_Report(String.format("PASS - The value %d and %d are NOT identical.", unit1, unit2));
	    	Ref_testPassed++;
	    } else {
	    	connectTo_Report(String.format("PASS - The value %d and %d are identical.", unit1, unit2));
	    	Ref_testFailed++;
	    }
  }

  public static void scan_CharAt(String unit1, String unit2) {
	  try {
	        char r1 = unit1.charAt(6);
	        char r2 = unit2.charAt(6);
	        if (unit1.length() >= 6 && unit2.length() >= 6) {
	            if (Character.isDigit(r1) || Character.isDigit(r2)) {
	                connectTo_Report(String.format("FAIL - %s and %s contains a combination of letters and numerals", 
	                		unit1, unit2));
	                Ref_testFailed++;
	            } else if (unit1.charAt(6) == r1 && unit2.charAt(6) == r2) {
	                connectTo_Report(String.format("PASS - %s and %s include the expected characters", 
	                		unit1, unit2));
	                Ref_testPassed++;
	            } else {
	                connectTo_Report(String.format("FAIL - %s and %s do not include the expected characters", 
	                		unit1, unit2));
	                Ref_testFailed++;
	            }
	        } 
	    } catch (StringIndexOutOfBoundsException e) {
	    	connectTo_Report(String.format("FAIL - Each string must contain at least six letters.", unit1, unit2));
            Ref_testFailed++;
	       
	    }
  }
  public static void scanDivisible_byte(byte unit1, byte unit2) {
	  byte fig1 = (byte) unit1;
	  byte fig2 = (byte) unit2;
	  try {
	    if (fig2 % 2 != 0) {
	      connectTo_Report(String.format("FAIL - %d is not divisible by 2", fig2));
	      Ref_testFailed++;
	      return;
	    }else if (fig1 % fig2 == 0) {
	      connectTo_Report(String.format("PASS - %d divide by %d is equal to 0", fig1, fig2));
	      Ref_testPassed++;
	    } else {
	      connectTo_Report(String.format("FAIL - %d divide by %d is not equal to 0", fig1, fig2));
	      Ref_testFailed++;
	    }
	  } catch (ArithmeticException e) {
		  connectTo_Report(String.format("FAIL - Cannot divide by zero"));
		  Ref_testFailed++;
	  }
  }
  
  public static void scan_Range(byte unit1, byte min_unit, byte max_unit) {
	    if (unit1 >= min_unit && unit1 <= max_unit) {
	    	connectTo_Report(String.format("PASS - Value %d is within the range of %d and %d.", 
	    			unit1, min_unit, max_unit));
	    	Ref_testPassed++;
	    } else {
	    	connectTo_Report(String.format("FAIL - Value %d is not in the range of %d and %d.", 
	    			unit1, min_unit, max_unit));
	    	Ref_testFailed++;
	    }
  }

  public static void ref_report() {
	System.out.printf("KUNIT - Nethmini Fernando - 2049342\n");
	System.out.printf("\nNumber of Passed Unit Test :%d \n",Ref_testPassed);
	System.out.printf("Number of Failed Unit Test :%d \n",Ref_testFailed);
    System.out.println();
    
    for (String ref_check : Ref_Checks) {
      System.out.println(ref_check);
    }
  }
}
