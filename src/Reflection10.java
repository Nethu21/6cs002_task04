import java.lang.reflect.*;

public class Reflection10 {
  public static void main(String[] args) throws Exception {
    Reflector_main echo = new Reflector_main();
    Method ref_m = echo.getClass().getDeclaredMethod("setE", byte.class);
    ref_m.setAccessible(true);
    ref_m.invoke(echo, (byte) 21);
    System.out.println(echo);
  }
}
