package Testing;
import static ref_KUnit.Ref_Kunit.*;
import java.lang.reflect.*;
public class Ref_Testing_kunit {

  void checkConstructorAndAccess(){
	  Reflector_main s = new Reflector_main((byte)25, (byte)45);
	  scan_EqualVals(s.getE(), (byte)25);
	  scan_EqualVals(s.getN(), (byte)-65);
	  scan_NotEqualVals(s.getN(), (byte)-12);    
	  scan_NotEqualVals(s.getN(), (byte)45);    
  }
  void checkStringCharAtValues(){
		Reflector_main s = new Reflector_main((byte)25, (byte)45);
		  scan_CharAt("Nethmini", "Fernando");
		  s = new Reflector_main((byte)25, (byte)0);
		  scan_CharAt("REF_0022", "EMK0987");
		  s = new Reflector_main((byte)25, (byte)0);
		  scan_CharAt("E", "KUM");
		  
  }
  void check_squareE(){
		Reflector_main s = new Reflector_main((byte)25, (byte)45);
	    s.sq_E();
	    scan_EqualVals(s.getE(),(byte) 25);
  }
	
	void checkDivisibleValues(){
		Reflector_main s = new Reflector_main((byte)25, (byte)45);
		scanDivisible_byte(s.getE(), s.getN());
		s = new Reflector_main((byte)8, (byte)2);
		scanDivisible_byte(s.getE(), s.getN());
		s = new Reflector_main((byte)-15, (byte)2);
		scanDivisible_byte(s.getE(), s.getN());
		s = new Reflector_main((byte)75, (byte)-0);
		scanDivisible_byte(s.getE(), s.getN());
	}
	
	void checkRangebyte() {
	    Reflector_main s = new Reflector_main((byte) 25, (byte) 0);
		scan_Range(s.getE(), (byte) 5, (byte) 50);
		s = new Reflector_main((byte) 21, (byte) 0);
		scan_Range(s.getE(), (byte) -10, (byte) 10);
	}

	public static void main(String[] args) {
		Ref_Testing_kunit ts = new Ref_Testing_kunit();
		ts.checkConstructorAndAccess();
		ts.checkStringCharAtValues();
		ts.checkDivisibleValues();
	    ts.check_squareE();
	    ts.checkRangebyte();
	    
	    Reflector_main s = new Reflector_main((byte)90, (byte)21);
	    try {
	      Field fields_r = s.getClass().getDeclaredField("ref02");
	      fields_r.setAccessible(true);
	      scan_EqualVals(fields_r.getByte(s), (byte)21);
	    }
	    catch (NoSuchFieldException | IllegalAccessException e) {
	      e.printStackTrace();
	    }
	    ref_report();
  }
}
